<?php

namespace App\Controller;

class Recall extends \App\Page {

    public function action_index() {
        if ($this->pixie->auth->user() != null) {
            $this->view->username = $this->pixie->auth->user()->name;
            $this->view->userlastname = $this->pixie->auth->user()->lastname;
        } else {
            $this->redirect('/');
        }

        $this->view->subview = 'recall';
        $this->view->alertMessage = null;

        $this->view->vr_count = $this->pixie->orm->get('recall')->count_all();
        $this->view->recall = $this->pixie->orm->get('recall')->find_all();
    }

    public function action_add()
    {
        if ($this->pixie->auth->user() != null) {
            $this->view->username = $this->pixie->auth->user()->name;
            $this->view->userlastname = $this->pixie->auth->user()->lastname;
        } else {
            $this->redirect('/');
        }

        $this->view->subview = 'recall_add';
        $this->view->alertMessage = null;

        if($this->request->method == 'POST'){
            $v = $this->pixie->orm->get('recall');

            $photo = null;

            if($_FILES['photo']['name'])
            {
                $valid_file = true;

                if(!$_FILES['photo']['error'])
                {
                    $new_file_name = (strtolower($_FILES['photo']['name']));
                    if($_FILES['photo']['size'] > (8024000))
                    {
                        $valid_file = false;
                        $photo = null;
                    }

                    if($valid_file)
                    {
                        //$new_file_name = translit($new_file_name);
                        move_uploaded_file($_FILES['photo']['tmp_name'], 'img/uploads/recall/'.$new_file_name);
                        $photo = $new_file_name;
                    }
                }
                else
                {
                    $photo = null;
                }
            }

            if($photo != null) {
                $ph = explode('.', $photo);

                $image = $this->pixie->image->read('img/uploads/recall/'.$photo);
                $image->resize(296,225);
                $image->save('img/uploads/recall/'.($ph[0].'_mini').$ph[1]);

                $image = $this->pixie->image->read('img/uploads/recall/'.$photo);
                $image->resize(299,298);
                $image->save('img/uploads/recall/'.($ph[0].'_big').$ph[1]);

                unlink('img/uploads/recall/'.$photo);

                $v->image = $photo;
                $v->title = $this->request->post('title');
                $v->text = $this->request->post('text');
                $v->who = $this->request->post('who');

                $v->save();

                $this->view->alertMessage = null;
            }else{
                $this->view->alertMessage = "Не возможно загрузить Вашу фотографию! Фотография не должна быть > 8мб";
            }

            if($photo != null) {
                $this->redirect('/recall');
            }else{
                $this->redirect('/recall/add/?error=1');
            }
        }
    }

    public function action_edit()
    {
        if ($this->pixie->auth->user() != null) {
            $this->view->username = $this->pixie->auth->user()->name;
            $this->view->userlastname = $this->pixie->auth->user()->lastname;
        } else {
            $this->redirect('/');
        }

        $id = $this->request->param('id');

        $this->view->subview = 'recall_edit';
        $this->view->alertMessage = null;
        $this->view->_id = $id;
        $this->view->recall = $this->pixie->orm->get('recall')->where('id','=',$id)->find();

        if($this->request->method == "POST") {
            $recall = $this->pixie->orm->get('recall')->where('id', '=', $id)->find();
            if ($recall->loaded()) {
                $recall->title = $this->request->post('title');
                $recall->text = $this->request->post('text');
                $recall->who = $this->request->post('who');

                $photo = "";

                if ($_FILES['photo']['name']) {
                    $valid_file = true;

                    if (!$_FILES['photo']['error']) {
                        $new_file_name = (strtolower($_FILES['photo']['name']));
                        if ($_FILES['photo']['size'] > (8024000)) {
                            $valid_file = false;
                            $photo = null;
                        }

                        if ($valid_file) {
                            if ($this->request->post('prev_pic') != '') {
                                $ph = explode('.',  $this->request->post('prev_pic'));
                                unlink('img/uploads/recall/'.($ph[0].'_mini').$ph[1]);
                                unlink('img/uploads/recall/'.($ph[0].'_big').$ph[1]);
                            }
                            //$new_file_name = translit($new_file_name);
                            move_uploaded_file($_FILES['photo']['tmp_name'], 'img/uploads/recall/' . $new_file_name);
                            $photo = $new_file_name;
                        }
                    } else {
                        $photo = null;
                    }
                } elseif ($this->request->post('prev_pic') != '' && !$_FILES['photo']['name']) {
                    $photo = $this->request->post('prev_pic');
                }

                $ph = explode('.', $photo);

                $image = $this->pixie->image->read('img/uploads/recall/'.$photo);
                $image->resize(296,225);
                $image->save('img/uploads/recall/'.($ph[0].'_mini').$ph[1]);

                $image = $this->pixie->image->read('img/uploads/recall/'.$photo);
                $image->resize(299,298);
                $image->save('img/uploads/recall/'.($ph[0].'_big').$ph[1]);

                unlink('img/uploads/recall/'.$photo);

                $recall->image = $photo;

                $recall->save();
                $this->view->alertMessage = null;

                $this->redirect('/recall');
            }
        }
    }

    public function action_delete()
    {
        if ($this->pixie->auth->user() != null) {
            $this->view->username = $this->pixie->auth->user()->name;
            $this->view->userlastname = $this->pixie->auth->user()->lastname;
        } else {
            $this->redirect('/');
        }

        $id = $this->request->param('id');
        if ($id) {
            $d = $this->pixie->orm->get('recall')->where('id','=',$id)->find();
            if($d->image != '') {
                $ph = explode('.', $d->image);
                unlink('img/uploads/recall/'.($ph[0].'_mini').$ph[1]);
                unlink('img/uploads/recall/'.($ph[0].'_big').$ph[1]);
            }
            $d->delete();
        }else{
            $_dell = $this->request->post('dell');

            foreach($_dell as $dell){
                $d = $this->pixie->orm->get('recall')->where('id','=',$dell)->find();
                if($d->image != '') {
                    $ph = explode('.', $d->image);
                    unlink('img/uploads/recall/'.($ph[0].'_mini').$ph[1]);
                    unlink('img/uploads/recall/'.($ph[0].'_big').$ph[1]);
                }
                $d->delete();
            }
        }

        $this->redirect('/recall');
    }

}
