<?php

namespace App\Controller;

class Common extends \App\Page {

    public function action_index()
    {
        if ($this->pixie->auth->user() != null) {
            $this->view->username = $this->pixie->auth->user()->name;
            $this->view->userlastname = $this->pixie->auth->user()->lastname;
        } else {
            $this->redirect('/');
        }

        $this->view->subview = 'common';

        $this->view->comm = $this->pixie->orm->get('common')->find_all();
        $this->view->comm_count = $this->pixie->orm->get('common')->count_all();
        if(($this->request->method == "GET")&&($this->request->get("error") == '1')) {
            $this->view->alertMessage = "Вы не можете удалить общую зону, если общее их количество <= 3";
        }else{
            $this->view->alertMessage = null;
        }
    }

    public function action_add()
    {

        if ($this->pixie->auth->user() != null) {
            $this->view->username = $this->pixie->auth->user()->name;
            $this->view->userlastname = $this->pixie->auth->user()->lastname;
        } else {
            $this->redirect('/');
        }

        $this->view->subview = "common_add";

        if(($this->request->method == "GET")&&($this->request->get("error") == '1')) {
            $this->view->alertMessage = "Не возможно загрузить Вашу фотографию! Фотография не должна быть > 8мб";
        }else{
            $this->view->alertMessage = null;
        }

        $photo = "";

        if($this->request->method == "POST"){
            if($_FILES['photo']['name'])
            {
                $valid_file = true;

                if(!$_FILES['photo']['error'])
                {
                    $new_file_name = (strtolower($_FILES['photo']['name']));
                    if($_FILES['photo']['size'] > (8024000))
                    {
                        $valid_file = false;
                        $photo = null;
                    }

                    if($valid_file)
                    {
                        //$new_file_name = translit($new_file_name);
                        move_uploaded_file($_FILES['photo']['tmp_name'], 'img/uploads/common/'.$new_file_name);
                        $photo = $new_file_name;
                    }
                }
                else
                {
                    $photo = null;
                }
            }

            if($photo != null) {
                $image = $this->pixie->image->read('img/uploads/common/'.$photo);
                $image->resize(294,294);
                unlink('img/uploads/common/'.$photo);
                $image->save('img/uploads/common/'.$photo);

                $common = $this->pixie->orm->get('common');
                $common->title = $this->request->post('title');
                $common->descr = $this->request->post('descr');
                $common->image = $photo;

                $common->save();
                $this->view->alertMessage = null;
            }else{
                $this->view->alertMessage = "Не возможно загрузить Вашу фотографию! Фотография не должна быть > 8мб";
            }

            if($photo != null) {
                $this->redirect('/common');
            }else{
                $this->redirect('/common/add/?error=1');
            }
        }
    }

    public function action_edit()
    {
        if ($this->pixie->auth->user() != null) {
            $this->view->username = $this->pixie->auth->user()->name;
            $this->view->userlastname = $this->pixie->auth->user()->lastname;
        } else {
            $this->redirect('/');
        }

        $id = $this->request->param('id');
        $this->view->subview = 'common_edit';
        $this->view->alertMessage = null;

        $this->view->comm = $this->pixie->orm->get('common')->where('id','=',$id)->find();

        if($this->request->method == "POST"){
            $comm = $this->pixie->orm->get('common')->where('id','=',$id)->find();
            if($comm->loaded()){
                $comm->title = $this->request->post('title');
                $comm->descr = $this->request->post('descr');

                $photo = "";

                if($_FILES['photo']['name'])
                {
                    $valid_file = true;

                    if(!$_FILES['photo']['error'])
                    {
                        $new_file_name = (strtolower($_FILES['photo']['name']));
                        if($_FILES['photo']['size'] > (8024000))
                        {
                            $valid_file = false;
                            $photo = null;
                        }

                        if($valid_file)
                        {
                            if($this->request->post('prev_pic') != '') {
                                if(file_exists("img/uploads/common/".$this->request->post('prev_pic')))
                                    unlink("img/uploads/common/".$this->request->post('prev_pic'));
                            }
                            //$new_file_name = translit($new_file_name);
                            move_uploaded_file($_FILES['photo']['tmp_name'], 'img/uploads/common/'.$new_file_name);
                            $photo = $new_file_name;
                        }
                    }
                    else
                    {
                        $photo = null;
                    }
                }elseif($this->request->post('prev_pic') != '' && !$_FILES['photo']['name']){
                    $photo = $this->request->post('prev_pic');
                }

                $image = $this->pixie->image->read('img/uploads/common/'.$photo);
                $image->resize(294,294);
                unlink('img/uploads/common/'.$photo);
                $image->save('img/uploads/common/'.$photo);
                $comm->image = $photo;

                $comm->save();
                $this->view->alertMessage = null;

                $this->redirect('/common/edit/'.$id);
            }
        }
    }

    public function action_delete()
    {
        if ($this->pixie->auth->user() != null) {
            $this->view->username = $this->pixie->auth->user()->name;
            $this->view->userlastname = $this->pixie->auth->user()->lastname;
        } else {
            $this->redirect('/');
        }

        $id = $this->request->param('id');

        if($this->pixie->orm->get('common')->count_all() <= 3)
        {
            $this->redirect('/common?error=1');
            return;
        }

        if ($id) {
            $d = $this->pixie->orm->get('common')->where('id','=',$id)->find();
            if($d->image != '') {
                unlink("img/uploads/common/".$d->image);
            }
            $d->delete();
        }else{
            $_dell = $this->request->post('dell');

            if($this->pixie->orm->get('common')->count_all() - count($_dell) < 3){
                $this->redirect('/common?error=1');
                return;
            }

            foreach($_dell as $dell){
                $d = $this->pixie->orm->get('common')->where('id','=',$dell)->find();
                if($d->image != '') {
                    unlink("img/uploads/common/".$d->image);
                }
                $d->delete();
            }
        }

        $this->redirect('/common');
    }
}
