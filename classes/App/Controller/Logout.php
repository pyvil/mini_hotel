<?php

namespace App\Controller;

class Logout extends \App\Page {

    public function action_index() {
        $this->pixie->auth->logout();
        $this->redirect('/');
    }

}
