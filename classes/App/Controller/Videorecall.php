<?php

namespace App\Controller;

class Videorecall extends \App\Page {

    public function action_index() {
        if ($this->pixie->auth->user() != null) {
            $this->view->username = $this->pixie->auth->user()->name;
            $this->view->userlastname = $this->pixie->auth->user()->lastname;
        } else {
            $this->redirect('/');
        }

        $this->view->subview = 'videorecall';
        $this->view->alertMessage = null;

        $this->view->vr_count = $this->pixie->orm->get('videorecall')->count_all();
        $this->view->videorecall = $this->pixie->orm->get('videorecall')->find_all();


    }

    public function action_add()
    {
        if ($this->pixie->auth->user() != null) {
            $this->view->username = $this->pixie->auth->user()->name;
            $this->view->userlastname = $this->pixie->auth->user()->lastname;
        } else {
            $this->redirect('/');
        }

        $this->view->subview = 'videorecall_add';
        $this->view->alertMessage = null;

        if($this->request->method == 'POST'){
            $v = $this->pixie->orm->get('videorecall');
            $v->title = $this->request->post('title');
            $v->src = $this->request->post('src');
            $v->save();
        }
    }

    public function action_edit()
    {
        if ($this->pixie->auth->user() != null) {
            $this->view->username = $this->pixie->auth->user()->name;
            $this->view->userlastname = $this->pixie->auth->user()->lastname;
        } else {
            $this->redirect('/');
        }

        $id = $this->request->param('id');

        $this->view->subview = 'videorecall_edit';
        $this->view->alertMessage = null;
        $this->view->_id = $id;
        $this->view->recall = $this->pixie->orm->get('videorecall')->where('id','=',$id)->find();

        if($this->request->method == 'POST'){
            $v = $this->pixie->orm->get('videorecall')->where('id','=', $id)->find();
            if($v->loaded()) {
                $v->title = $this->request->post('title');
                $v->src = $this->request->post('src');
                $v->save();
            }
        }
    }

    public function action_delete()
    {
        if ($this->pixie->auth->user() != null) {
            $this->view->username = $this->pixie->auth->user()->name;
            $this->view->userlastname = $this->pixie->auth->user()->lastname;
        } else {
            $this->redirect('/');
        }

        $id = $this->request->param('id');
        if($id){
            $this->pixie->orm->get('videorecall')->where('id','=', $id)->find()->delete();
        }else{
            $_dell = $this->request->post('dell');
            foreach($_dell as $dell){
                $this->pixie->orm->get('videorecall')->where('id','=', $dell)->find()->delete();
            }
        }
        $this->redirect('/videorecall');
    }

}
