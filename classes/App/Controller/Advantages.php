<?php

namespace App\Controller;

class Advantages extends \App\Page {

    public function action_index() {
        if ($this->pixie->auth->user() != null) {
            $this->view->username = $this->pixie->auth->user()->name;
            $this->view->userlastname = $this->pixie->auth->user()->lastname;
        } else {
            $this->redirect('/');
        }

        $this->view->subview = 'advantages';
        $this->view->advs = $this->pixie->orm->get('advantages')->find_all();
        $this->view->advs_count = $this->pixie->orm->get('advantages')->count_all();
        if(($this->request->method == "GET")&&($this->request->get("error") == '1')) {
            $this->view->alertMessage = "Вы не можете удалить преимущесво, если общее их количество <= 5";
        }else{
            $this->view->alertMessage = null;
        }
    }

    public function action_add()
    {
        if ($this->pixie->auth->user() != null) {
            $this->view->username = $this->pixie->auth->user()->name;
            $this->view->userlastname = $this->pixie->auth->user()->lastname;
        } else {
            $this->redirect('/');
        }

        $this->view->subview = 'advantages_add';

        if ($this->request->method == 'POST') {
            $adv = $this->pixie->orm->get('advantages');

            $adv->text = $this->request->post('text');
            $adv->state = $this->request->post('avaliable');

            $adv->save();
        }
    }

    public function action_edit()
    {
        if ($this->pixie->auth->user() != null) {
            $this->view->username = $this->pixie->auth->user()->name;
            $this->view->userlastname = $this->pixie->auth->user()->lastname;
        } else {
            $this->redirect('/');
        }

        $id = $this->request->param('id');
        $this->view->subview = 'advantages_edit';
        $this->view->adv = $this->pixie->orm->get('advantages', $id)->as_array();
        $adv = $this->pixie->orm->get('advantages', $id)->find();

        if ($this->request->method == 'GET') {
            $id = $this->request->get('id');
            if($this->request->get('action') == "state"){
                $state = $this->request->get('v');
                $adv = $this->pixie->orm->get('advantages')->where('id',$id)->find();
                if($adv->loaded()){
                    $adv->state = $state == '1' ? '0' : '1';
                    $adv->save();
                    $this->redirect('/advantages');
                }
            }
            return;
        }

        if ($this->request->method == 'POST') {
            if($adv->loaded()){
                $adv->text = $this->request->post('text');
                $adv->state = $this->request->post('avaliable');
                $adv->save();

                $this->redirect('/advantages');
            }
        }
    }

    public function action_delete()
    {

        if ($this->pixie->auth->user() != null) {
            $this->view->username = $this->pixie->auth->user()->name;
            $this->view->userlastname = $this->pixie->auth->user()->lastname;
        } else {
            $this->redirect('/');
        }

        $id = $this->request->param('id');
        if($this->pixie->orm->get('advantages')->count_all() <= 5)
        {
            $this->redirect('/advantages?error=1');
            return;
        }
        if(!$id){
            if ($this->request->method == 'POST') {
                $_dell = $this->request->post('dell');

                if($this->pixie->orm->get('advantages')->count_all() - count($_dell) < 5){
                    $this->redirect('/advantages?error=1');
                    return;
                }

                foreach($_dell as $dell){
                    $this->pixie->orm->get('advantages', $dell)->delete();
                }
            }
        }else{
            $this->pixie->orm->get('advantages', $id)->delete();
        }
        $this->redirect('/advantages');
    }

}
