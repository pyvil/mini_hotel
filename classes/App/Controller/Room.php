<?php

namespace App\Controller;

class Room extends \App\Page {

    protected $valid_formats = array("jpg", "png", "gif", "bmp","jpeg","PNG","JPG","JPEG","GIF","BMP");

    public function getExtension($str)
    {
        $i = strrpos($str,".");
        if (!$i) { return ""; }
        $l = strlen($str) - $i;
        $ext = substr($str,$i+1,$l);
        return $ext;
    }

    public function action_index()
    {
        if ($this->pixie->auth->user() != null) {
            $this->view->username = $this->pixie->auth->user()->name;
            $this->view->userlastname = $this->pixie->auth->user()->lastname;
        } else {
            $this->redirect('/');
        }

        $this->view->subview = 'room';

        $this->view->room = $this->pixie->orm->get('room')->find_all();
        $this->view->room_count = $this->pixie->orm->get('room')->count_all();
        if(($this->request->method == "GET")&&($this->request->get("error") == '1')) {
            $this->view->alertMessage = "Вы не можете удалить общую зону, если общее их количество <= 3";
        }else{
            $this->view->alertMessage = null;
        }
    }

    public function action_add()
    {

        if ($this->pixie->auth->user() != null) {
            $this->view->username = $this->pixie->auth->user()->name;
            $this->view->userlastname = $this->pixie->auth->user()->lastname;
        } else {
            $this->redirect('/');
        }

        $this->view->subview = 'room_add';
        $this->view->alertMessage = null;

        if($this->request->method == "POST")
        {
            $room = $this->pixie->db->query('insert')->table('rooms')->data(
                array(
                    "title_little" => $this->request->post('title_little'),
                    "title" => $this->request->post('title'),
                    "descr" => $this->request->post('descr'),
                    "descr_all" => $this->request->post('descr_all'),
                    "price" => $this->request->post('price'),
                    "cat" => $this->request->post('avaliable'),
                )
            )->execute();

            $room_id = $this->pixie->db->insert_id();

            foreach($this->request->post('roomhave') as $rh) {
                $roomHave = $this->pixie->db->query('insert')->table('roomhaves')->data(
                    array(
                        'text' => $rh,
                        'id_room' => $room_id
                    )
                )->execute();
            }

            foreach($_FILES['photo']['name'] as $k=>$f) {
                $photo = "";
                if (!$_FILES['photo']['error'][$k]) {
                    if (is_uploaded_file($_FILES['photo']['tmp_name'][$k])) {
                        $new_file_name = (strtolower($_FILES['photo']['name'][$k]));
                        if($_FILES['photo']['size'][$k] > (8024000))
                        {
                            $photo = "";
                        }
                        if (!move_uploaded_file($_FILES['photo']['tmp_name'][$k], 'img/uploads/room/'.$new_file_name)) {
                            $photo = "";
                        }else{
                            $photo = $new_file_name;
                        }
                        if($photo != ''){
                            $ph = explode('.', $photo);

                            $image = $this->pixie->image->read('img/uploads/room/'.$photo);
                            $image->resize(107,79);
                            $image->save('img/uploads/room/'.($ph[0].'_mini.').$ph[1]);

                            $image = $this->pixie->image->read('img/uploads/room/'.$photo);
                            $image->resize(203,108);
                            $image->save('img/uploads/room/'.($ph[0].'_middle.').$ph[1]);

                            $image = $this->pixie->image->read('img/uploads/room/'.$photo);
                            $image->resize(386,283);
                            $image->save('img/uploads/room/'.($ph[0].'_big.').$ph[1]);

                            unlink('img/uploads/room/'.$photo);
                        }
                        $this->pixie->db->query('insert')->table('roompics')->data(
                            array(
                                'image' => $photo,
                                'id_room' => $room_id
                            )
                        )->execute();
                    }
                }
            }
            $this->redirect('/room/add/');
        }
    }

    public function action_edit()
    {
        if ($this->pixie->auth->user() != null) {
            $this->view->username = $this->pixie->auth->user()->name;
            $this->view->userlastname = $this->pixie->auth->user()->lastname;
        } else {
            $this->redirect('/');
        }

        //$this->view->subview = 'room_add';

        $id = $this->request->param('id');
        $this->view->subview = 'room_edit';
        $this->view->alertMessage = null;
        $this->view->rooms = $this->pixie->orm->get('room')->where('id','=',$id)->find();
        $this->view->_id = $id;

        if($this->request->method == "POST"){
            $room = $this->pixie->orm->get('room')->where('id', '=', $id)->find();
            if($room->loaded()){
                $room->title_little = $this->request->post('title_little');
                $room->title = $this->request->post('title');
                $room->descr = $this->request->post('descr');
                $room->descr_all = $this->request->post('descr_all');
                $room->price = $this->request->post('price');
                $room->cat = $this->request->post('avaliable');
                $room->save();
            }
            if($this->request->post('roomhave'))
                foreach($this->request->post('roomhave') as $rh) {
                    $roomHave = $this->pixie->db->query('insert')->table('roomhaves')->data(
                        array(
                            'text' => $rh,
                            'id_room' => $id
                        )
                    )->execute();
                }

            if($_FILES['photo']['name'])
            foreach($_FILES['photo']['name'] as $k=>$f) {
                $photo = "";
                if (!$_FILES['photo']['error'][$k]) {
                    if (is_uploaded_file($_FILES['photo']['tmp_name'][$k])) {
                        $new_file_name = (strtolower($_FILES['photo']['name'][$k]));
                        if($_FILES['photo']['size'][$k] > (8024000))
                        {
                            $photo = "";
                            continue;
                        }
                        if (!move_uploaded_file($_FILES['photo']['tmp_name'][$k], 'img/uploads/room/'.$new_file_name)) {
                            $photo = "";
                        }else{
                            $photo = $new_file_name;
                        }


                            if($photo != ''){
                                $ph = explode('.', $photo);

                                $image = $this->pixie->image->read('img/uploads/room/'.$photo);
                                $image->resize(107,79);
                                $image->save('img/uploads/room/'.($ph[0].'_mini.').$ph[1]);

                                $image = $this->pixie->image->read('img/uploads/room/'.$photo);
                                $image->resize(203,108);
                                $image->save('img/uploads/room/'.($ph[0].'_middle.').$ph[1]);

                                $image = $this->pixie->image->read('img/uploads/room/'.$photo);
                                $image->resize(386,283);
                                $image->save('img/uploads/room/'.($ph[0].'_big.').$ph[1]);

                                unlink('img/uploads/room/'.$photo);
                            }

                            if($k == 0){
                                if($this->request->post('prev_pic') != '' && !$_FILES['photo']['name'][0]){
                                    continue;
                                }
                                $ph = explode('.', $this->request->post('prev_pic'));
                                if(file_exists('img/uploads/room/'.($ph[0].'_mini.').$ph[1])){
                                    unlink('img/uploads/room/'.($ph[0].'_mini.').$ph[1]);
                                    unlink('img/uploads/room/'.($ph[0].'_middle.').$ph[1]);
                                    unlink('img/uploads/room/'.($ph[0].'_big.').$ph[1]);
                                }
                                $p = $this->pixie->orm->get('roompic')->where('id','=',$this->request->post('prev_id'))->find();
                                if($p->loaded()){
                                    $p -> image = $photo;
                                    $p->save();
                                }
                            }else{
                                $p = $this->pixie->orm->get('roompic');
                                $p->image = $photo;
                                $p->id_room = $id;
                                $p->save();
                                /*$this->pixie->db->query('insert')->table('roompics')->data(
                                    array(
                                        'image' => $photo,
                                        'id_room' => $id
                                    )
                                )->execute();*/
                            }
                        //}
                    }
                }
            }
            $this->redirect('/room/');
        }
    }

    public function action_delete()
    {
        if ($this->pixie->auth->user() != null) {
            $this->view->username = $this->pixie->auth->user()->name;
            $this->view->userlastname = $this->pixie->auth->user()->lastname;
        } else {
            $this->redirect('/');
        }

        $id = $this->request->param('id');
        if($id){

            $pics = $this->pixie->orm->get('roompic')->where('id_room','=',$id)->find_all();
            foreach($pics as $pic){
                    if($pic->image != '') {
                        $ph = explode('.', $pic->image);
                        if(file_exists('img/uploads/room/' . ($ph[0] . '_mini.') . $ph[1])) {
                            unlink('img/uploads/room/' . ($ph[0] . '_mini.') . $ph[1]);
                            unlink('img/uploads/room/' . ($ph[0] . '_middle.') . $ph[1]);
                            unlink('img/uploads/room/' . ($ph[0] . '_big.') . $ph[1]);
                        }
                    }
            }

            $room = $this->pixie->orm->get('room')->where('id','=',$id)->find();

            $room->roomhave->delete_all();
            $room->roompic->delete_all();
            $room->delete();

        }else{
            $_dell = $this->request->post('dell');

            foreach($_dell as $dell){
                $pics = $this->pixie->orm->get('roompic')->where('id_room','=',$dell)->find_all();
                foreach($pics as $pic){
                    if($pic->image != '') {
                        $ph = explode('.', $pic->image);
                        if(file_exists('img/uploads/room/' . ($ph[0] . '_mini.') . $ph[1])) {
                            unlink('img/uploads/room/' . ($ph[0] . '_mini.') . $ph[1]);
                            unlink('img/uploads/room/' . ($ph[0] . '_middle.') . $ph[1]);
                            unlink('img/uploads/room/' . ($ph[0] . '_big.') . $ph[1]);
                        }
                    }
                }

                $room = $this->pixie->orm->get('room')->where('id','=',$dell)->find();

                $room->roomhave->delete_all();
                $room->roompic->delete_all();
                $room->delete();

            }
        }
        $this->redirect('/room/');
    }
}
