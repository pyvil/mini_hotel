<?php
namespace App\Controller;

class Settings extends \App\Page {

    public function action_index() {

        if ($this->pixie->auth->user() != null) {
            $this->view->username = $this->pixie->auth->user()->name;
            $this->view->userlastname = $this->pixie->auth->user()->lastname;
        } else {
            $this->redirect('/administrator');
        }

        $this->view->subview = 'settings';
        $count = $this->pixie->orm->get('siteSetting')->count_all();
        $this->view->settings = ($count == 0) ? null : $this->pixie->orm->get('siteSetting')->find_all();

        if ($this->request->method == 'POST') {
            if($this->view->settings == null){
                $setting = $this->pixie->orm->get('siteSetting');
                $setting->title = $this->request->post('title');
                $setting->descr = $this->request->post('descr');
                $setting->usp = $this->request->post('usp');
                $setting->phone = $this->request->post('phone');
                $setting->alt_phone = $this->request->post('alt_phone');
                $setting->avaliable = $this->request->post('avaliable');
                $setting->address = $this->request->post('address');
                $setting->mail = $this->request->post('mail');

                $setting->save();


            }else{
                $setting = $this->pixie->orm->get('siteSetting')->where('id','=',$this->request->post('id'))->find();
                if($setting->loaded()):
                    $setting->title = $this->request->post('title');
                    $setting->descr = $this->request->post('descr');
                    $setting->usp = $this->request->post('usp');
                    $setting->phone = $this->request->post('phone');
                    $setting->alt_phone = $this->request->post('alt_phone');
                    $setting->avaliable = $this->request->post('avaliable');
                    $setting->address = $this->request->post('address');
                    $setting->mail = $this->request->post('mail');

                    $setting->save();
                endif;
            }

            $this->redirect('/settings');
        }
    }

}
