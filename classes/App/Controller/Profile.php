<?php

namespace App\Controller;

class Profile extends \App\Page {

    public function action_index() {

        if ($this->pixie->auth->user() != null) {
            $this->view->username = $this->pixie->auth->user()->name;
            $this->view->userlastname = $this->pixie->auth->user()->lastname;
        } else {
            $this->redirect('/');
        }

        $this->view->subview = 'profile';
        $this->view->profile = $this->pixie->orm->get('index')->where('id','=',$this->pixie->auth->user()->id)->find();

        if($this->request->method == 'POST'){
            $pr = $this->pixie->orm->get('index')->where('id','=',$this->pixie->auth->user()->id)->find();

            if($pr->loaded()){
                $pr->name = $this->request->post('name');
                $pr->lastname = $this->request->post('lastname');
                $pr->mail = $this->request->post('mail');
                $pr->pass = $this->request->post('pass');

                $pr->save();

                $this->pixie->auth->user()->name = $this->request->post('name');
                $this->pixie->auth->user()->lastname = $this->request->post('lastname');
                $this->pixie->auth->user()->mail = $this->request->post('mail');
                $this->pixie->auth->user()->pass = $this->request->post('pass');

                $this->redirect('/profile');
            }

        }
    }

}
