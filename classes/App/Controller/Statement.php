<?php

namespace App\Controller;

class Statement extends \App\Page {

    public function action_index() {
        if ($this->pixie->auth->user() != null) {
            $this->view->username = $this->pixie->auth->user()->name;
            $this->view->userlastname = $this->pixie->auth->user()->lastname;
        } else {
            $this->redirect('/');
        }

        $this->view->subview = 'statement';

        $this->view->alertMessage = null;
        $this->view->state_count = $this->pixie->orm->get('statement')->count_all();
        $this->view->state = $this->pixie->orm->get('statement')->find_all();
    }

    public function action_view() {
        if ($this->pixie->auth->user() != null) {
            $this->view->username = $this->pixie->auth->user()->name;
            $this->view->userlastname = $this->pixie->auth->user()->lastname;
        } else {
            $this->redirect('/');
        }

        $id = $this->request->param('id');

        $this->view->subview = 'statement_view';

        $this->view->alertMessage = null;
        if($this->request->method == 'POST'){
            $tmp = $this->pixie->orm->get('statement')->where('id', '=', $id)->find();
            if ($tmp->loaded()) {
                $tmp->status = 0;
                $tmp->save();
            }
        }else {
            $tmp = $this->pixie->orm->get('statement')->where('id', '=', $id)->find();
            if ($tmp->loaded()) {
                $tmp->status = 1;
                $tmp->save();
            }
        }
        $this->view->st = $this->pixie->orm->get('statement')->where('id','=',$id)->find();
    }

}
