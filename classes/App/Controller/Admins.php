<?php

namespace App\Controller;

class Admins extends \App\Page {

    public function action_index() {
        if ($this->pixie->auth->user() != null) {
            $this->view->username = $this->pixie->auth->user()->name;
            $this->view->userlastname = $this->pixie->auth->user()->lastname;
        } else {
            $this->redirect('/');
        }

        $this->view->subview = 'admins';

        $this->view->admins = $this->pixie->orm->get('index')->find_all();
        $this->view->admins_count = $this->pixie->orm->get('index')->count_all();
    }

    public function action_add() {
        if ($this->pixie->auth->user() != null) {
            $this->view->username = $this->pixie->auth->user()->name;
            $this->view->userlastname = $this->pixie->auth->user()->lastname;
        } else {
            $this->redirect('/');
        }

        $this->view->subview = 'admin_add';

        if($this->request->method == 'POST'){
            $a = $this->pixie->orm->get('index');
            $a->name = $this->request->post('name');
            $a->lastname = $this->request->post('lastname');
            $a->mail = $this->request->post('mail');
            $a->pass = $this->request->post('pass');

            $a->save();

            $this->redirect('/admins');
        }
    }

    public function action_delete()
    {
        $id = $this->request->param('id');
        if($id){
            $this->pixie->orm->get('index')->where('id','=',$id)->find()->delete();
        }else{
            $_dell = $this->request->post('dell');
            foreach($_dell as $dell){
                $this->pixie->orm->get('index')->where('id','=',$dell)->find()->delete();
            }
        }
        $this->redirect('/admins');
    }

}
