<?php
namespace App\Model;

//PHPixie will guess the name of the table
//from the class name
class Room extends \PHPixie\ORM\Model {
    /**
     * @var array
     * relation between 3 tables, which mean: Room has some stuff in self and each Room has picture to preview
     */

    protected $has_many = array(
      'roomhave' => array(
          'model' => 'roomhave',
          'key'   => 'id_room'
      ),

       'roompic' => array(
           'model' => 'roompic',
           'key'   => 'id_room'
       )
    );
}