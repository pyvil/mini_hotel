<?php
namespace App\Model;

//PHPixie will guess the name of the table
//from the class name
class Statement extends \PHPixie\ORM\Model {
    protected $belongs_to = array(
        'wherefrom' => array(
            'model' => 'wherefrom',
            'key'   => 's_wherefrom'
        )
    );
}