<?php
return array(
    'default' => array(
        'model' => 'index',
        'login' => array(
            'password' => array(
                'login_field' => 'mail',
                'password_field' => 'pass',
                'hash_method' => false
            )
        ))
);
