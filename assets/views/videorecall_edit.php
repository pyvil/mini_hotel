<h1>Редактировать отзыв: <?=$_id?></h1>
<?if($alertMessage != null):?>
    <div class="errorMessage">
        <?=$alertMessage?>
    </div>
<?endif;?>
<div class="page review_comment">
    <form enctype="multipart/form-data" action="" method="post">
        <label>Видео (загрузите видео на <a href="https://www.youtube.com/" target="_blank">youtube.com</a>) и вставте ссылку на видео сюда:</label>
        <input type="text" id="src" required value="https://www.youtube.com/watch?v=<?=$recall->src?>">
        <img src="/img/loader.gif" style="display: none" id="loader">
        <label>Название:</label>
        <input type="text" name="title" id="title" required value="<?=$recall->title?>">
        <input type="hidden" name="src" value="<?=$recall->src?>" id="h_src">
        <input type="submit" name="submit" value="Добавить" >
        <div style="clear: both"></div>
    </form>
    <div class="view_img">
        <h2>Видео:</h2>
        <iframe width="460" height="315" src="https://www.youtube.com/embed/<?=$recall->src?>" frameborder="0" allowfullscreen></iframe>
    </div>
</div>