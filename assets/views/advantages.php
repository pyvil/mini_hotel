<h1>Наши преимущества</h1>
<div class="edit_recalls edit_our_works">
    <form method="post" action="advantages/delete/" class="delete_form">
        <?if($alertMessage != null):?>
            <div class="errorMessage">
                <?=$alertMessage?>
            </div>
        <?endif;?>
        <div class="page settings tabs panel">
            <a href="advantages/add/" class="subm_link">Новое преимущество</a>
            <div style="clear: both"></div>
        </div>
        <table>
            <?$i=0;?>
            <?if($advs_count == 0):?>
                <tr><th><h3>Данных для отображения нету :-(</h3></th></tr>
            <?else:?>
                <tr><th></th><th>#</th><th>Текст преимущества</th><th>Вкл/Выкл</th><th>Действия</th></tr>
                <?foreach($advs as $adv): $i++;?>
                <tr>
                    <td style="padding: 0; text-align: center"><input type="checkbox" name="dell[]" value="<?=$adv->id?>"></td>
                    <td><span><?=$i?></span></td>
                    <td><span><?=$adv->text?></span></td>
                    <td class="state">
                        <a href="advantages/edit/?id=<?=$adv->id?>&action=state&v=<?=$adv->state?>">
                            <img src="/img/<?=($adv->state == 1) ? "active" : "inactive"?>.png">
                        </a>
                    </td>
                    <td>
                        <a href="advantages/edit/<?=$adv->id?>"><img src="/img/edit.gif"></a>
                        <a href="advantages/delete/<?=$adv->id?>"><img src="/img/delete.gif"></a>
                    </td>
                </tr>
                <?endforeach;?>
            <?endif;?>
        </table>
        <input type="submit" name="submit" value="Удалить виделенное"/>
    </form>
</div>