<h1>Новый отзыв:</h1>
<?if($alertMessage != null):?>
    <div class="errorMessage">
        <?=$alertMessage?>
    </div>
<?endif;?>
<div class="page review_comment">
    <form enctype="multipart/form-data" action="" method="post">
        <label>Фотография:</label>
        <input type="file" name="photo" required onchange="viewAddImg(this,event)">
        <label>Автор:</label>
        <input type="text" name="who" id="who" required>
        <label>Название:</label>
        <input type="text" name="title" id="title" required>
        <label>Текст:</label>
        <textarea name="text" required></textarea>
        <input type="submit" name="submit" value="Добавить" >
        <div style="clear: both"></div>
    </form>
    <div class="view_img">
        <h2>Картинка, которая будет добавлена:</h2>
    </div>
</div>