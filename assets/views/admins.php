<h1>Администрация</h1>
<div class="edit_recalls edit_our_works">
    <form method="post" action="/admins/delete/" class="delete_form">
        <div class="page settings tabs panel">
            <a href="/admins/add/" class="subm_link">Добавить</a>
            <div style="clear: both"></div>
        </div>
        <table>
            <?$i=0;?>
            <?if($admins_count == 0):?>
                <tr><th><h3>Данных для отображения нету :-(</h3></th></tr>
            <?else:?>
                <tr><th></th><th>#</th><th>Имя</th><th>Фамилия</th><th>E-mail</th><th>Действия</th></tr>
                <?foreach($admins as $com): $i++;?>
                    <tr>
                        <td style="padding: 0; text-align: center"><input type="checkbox" name="dell[]" value="<?=$com->id?>"></td>
                        <td><span><?=$i?></span></td>
                        <td>
                            <span><?=$com->name?></span>
                        </td>
                        <td><span><?=$com->lastname?></span></td>
                        <td>
                            <span><?=$com->mail?></span>
                        </td>
                        <td>
                            <a href="/admins/delete/<?=$com->id?>"><img src="/img/delete.gif"></a>
                        </td>
                    </tr>
                <?endforeach;?>
            <?endif;?>
        </table>
        <input type="submit" name="submit" value="Удалить виделенное"/>
    </form>
</div>