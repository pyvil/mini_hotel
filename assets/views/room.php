<h1>Номера</h1>
<div class="edit_recalls edit_our_works">
    <form method="post" action="/room/delete/" class="delete_form">
        <?if($alertMessage != null):?>
            <div class="errorMessage">
                <?=$alertMessage?>
            </div>
        <?endif;?>
        <div class="page settings tabs panel">
            <a href="/room/add/" class="subm_link">Новый номер</a>
            <div style="clear: both"></div>
        </div>
        <table>
            <?$i=0;?>
            <?if($room_count == 0):?>
                <tr><th><h3>Данных для отображения нету :-(</h3></th></tr>
            <?else:?>
                <tr><th></th><th>#</th><th>Краткое название</th><th>Краткое описание</th><th>Цена</th><th>Категория</th></tr>
                <?foreach($room as $com): $i++;?>
                    <tr>
                        <td style="padding: 0; text-align: center"><input type="checkbox" name="dell[]" value="<?=$com->id?>"></td>
                        <td><span><?=$i?></span></td>
                        <td>
                            <span><?=$com->title_little?></span>
                        </td>
                        <td><span><?=$com->descr?></span></td>
                        <td>
                            <span><?=$com->price?></span>
                        </td>
                        <td>
                            <span><?=$com->cat?></span>
                        </td>
                        <td>
                            <a href="/room/edit/<?=$com->id?>"><img src="/img/edit.gif"></a>
                            <a href="/room/delete/<?=$com->id?>"><img src="/img/delete.gif"></a>
                        </td>
                    </tr>
                <?endforeach;?>
            <?endif;?>
        </table>
        <input type="submit" name="submit" value="Удалить виделенное"/>
    </form>
</div>