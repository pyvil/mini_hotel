<h1>Редактирование номера: <?=$_id?></h1>
<?if($alertMessage != null):?>
    <div class="errorMessage">
        <?=$alertMessage?>
    </div>
<?endif;?>

<?
/*echo "<pre>";
var_dump($rooms);
echo "</pre>";*/
?>

<form action="" method="post" enctype="multipart/form-data">
    <div class="page settings tabs panel">
        <input type="submit" name="submit" value="Сохранить" >
    </div>
    <div class="page settings tabs review_comment">
        <div class="tab">Краткое</div>
        <div class="left">
            <label>Фотография:</label>
            <input type="file" name="photo[]"  onchange="viewAddImg(this,event)">
            <label>Название:</label>
            <input type="text" name="title_little" id="title" required value="<?=$rooms->title_little?>">
            <label>Краткое описание:</label>
            <textarea name="descr" required ><?=$rooms->descr?></textarea>
        </div>
        <div class="view_img">
            <h2>Картинка, которая будет добавлена:</h2>
            <?
            $ph = explode('.', $rooms->roompic->limit(1)->find()->image);
            $photo = $ph[0].'_big.'.$ph[1];
            ?>
            <img src="/img/uploads/room/<?=$photo?>">
        </div>
        <input type="hidden" name="prev_pic" value="<?=$rooms->roompic->limit(1)->find()->image?>"/>
        <input type="hidden" name="prev_id" value="<?=$rooms->roompic->limit(1)->find()->id?>"/>
    </div>

    <div class="page settings tabs review_comment">
        <div class="tab">Полное</div>
        <div class="left">
            <label>Фотографии:</label>
            <div class="m_cont">
                <input type="file" name="photo[]" class="multiple" onchange='viewAddImg(this,event);'>
            </div>
            <div class="buttons">
                <div class="addInput">Добавить фотку</div>
            </div>
            <label>Название:</label>
            <input type="text" name="title" id="title" required value="<?=$rooms->title?>">
            <label>Описание:</label>
            <textarea name="descr_all" required><?=$rooms->descr?></textarea>
            <label>Цена:</label>
            <input type="text" name="price" id="price" value="<?=$rooms->price?>">
            <label>В номере есть: <br/>(нажмите на нужный пункт чтобы изменить и нажмите Enter, чтобы сохранить)</label>
            <ul class="list">
                <?foreach($rooms->roomhave->find_all() as $have):?>
                <div class='list_item'>
                    <li name="<?=$have->id?>"><?=$have->text?></li>
                    <span><img src='/img/delete.gif' onclick="ajaxDeleteHave(this,<?=$have->id?>)"></span>
                </div>
                <?endforeach;?>
            </ul>
            <input type="text" id="roomh">
            <div class="buttons">
                <div class="addInputHave" onclick="addHave('edit')">Добавить</div>
            </div>

            <label>Выбор категории: </label>
            <input type="radio" name="avaliable" id="avaliable"  value="1" <?if($rooms->cat == 1):?>checked<?endif;?>>
            <label for="avaliable" id="avaliable_1">1-а категория</label>
            <div></div>
            <input type="radio" name="avaliable" id="unavaliable" value="2" <?if($rooms->cat == 2):?>checked<?endif;?>>
            <label for="unavaliable" id="unavaliable_1">2-ая категория</label>
        </div>
        <div class="view_img_mult">
            <h2>Картинки, которые будут добавлены:</h2>
            <?foreach($rooms->roompic->where('id','>',$rooms->roompic->limit(1)->find()->id)->find_all() as $pic):?>
            <div class='blob_container' style="width: 180px;">
                <span class='delete' onclick='deletePicture(this,<?=$pic->id?>)'></span>
                <?
                $ph = explode('.', $pic->image);
                $photo = $ph[0].'_big.'.$ph[1];
                ?>
                <img src ='/img/uploads/room/<?=$photo?>'>
            </div>
            <?endforeach;?>
        </div>
    </div>
</form>