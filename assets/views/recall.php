<h1>Отзыв</h1>
<div class="edit_recalls edit_our_works">
    <form method="post" action="/recall/delete/" class="delete_form">
        <?if($alertMessage != null):?>
            <div class="errorMessage">
                <?=$alertMessage?>
            </div>
        <?endif;?>
        <div class="page settings tabs panel">
            <a href="/recall/add/" class="subm_link">Новый отзыв</a>
            <div style="clear: both"></div>
        </div>
        <table>
            <?$i=0;?>
            <?if($vr_count == 0):?>
                <tr><th><h3>Данных для отображения нету :-(</h3></th></tr>
            <?else:?>
                <tr><th></th><th>#</th><th>Изображение</th><th>Название</th><th>Текст</th><th>Автор</th><th>Действия</th></tr>
                <?foreach($recall as $vr): $i++;?>
                    <tr>
                        <td style="padding: 0; text-align: center"><input type="checkbox" name="dell[]" value="<?=$vr->id?>"></td>
                        <td><span><?=$i?></span></td>
                        <td>
                            <span><img src="/img/uploads/recall/<?=$vr->image?>"></span>
                        </td>
                        <td><span><?=$vr->title?></span></td>
                        <td><span><?=$vr->text?></span></td>
                        <td><span><?=$vr->who?></span></td>
                        <td>
                            <a href="/recall/edit/<?=$vr->id?>"><img src="/img/edit.gif"></a>
                            <a href="/recall/delete/<?=$vr->id?>"><img src="/img/delete.gif"></a>
                        </td>
                    </tr>
                <?endforeach;?>
            <?endif;?>
        </table>
        <input type="submit" name="submit" value="Удалить виделенное"/>
    </form>
</div>