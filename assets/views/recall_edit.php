<h1>Редактирование зоны: <i><?=$recall->title?></i></h1>
<?if($alertMessage != null):?>
    <div class="errorMessage">
        <?=$alertMessage?>
    </div>
<?endif;?>
<div class="page review_comment">
    <form enctype="multipart/form-data" action="" method="post">
        <label>Фотография:</label>
        <input type="file" name="photo" onchange="viewAddImg(this,event)">
        <label>Автор:</label>
        <input type="text" name="who" id="who" required value="<?=$recall->who?>">
        <label>Название:</label>
        <input type="text" name="title" id="title" required value="<?=$recall->title?>">
        <label>Текст:</label>
        <textarea name="text" required><?=$recall->text?></textarea>
        <input type="submit" name="submit" value="Добавить" >
        <input type="hidden" name="prev_pic" value="<?=$recall->image?>" >
        <div style="clear: both"></div>
    </form>
    <div class="view_img">
        <h2>Картинка, которая будет добавлена:</h2>
        <img src="/img/uploads/recall/<?=$recall->image?>" onclick="view(this)"/>
    </div>
</div>