<h1>Заявки</h1>
<div class="edit_recalls edit_our_works">
    <form method="post" action="/statement/view/" class="delete_form">
        <?if($alertMessage != null):?>
            <div class="errorMessage">
                <?=$alertMessage?>
            </div>
        <?endif;?>
        <table>
            <?$i=0;?>
            <?if($state_count == 0):?>
                <tr><th><h3>Данных для отображения нету :-(</h3></th></tr>
            <?else:?>
                <tr><th></th><th>#</th><th>Имя</th><th>Телефон</th><th>E-mail</th><th>Трансфер</th><th>Откуда</th><th>Статус</th><th>Дата</th><th>Время</th><th>Действия</th></tr>
                <?foreach($state as $com): $i++;?>
                    <?
                        $status = $com->status == 1 ? "Прочитано" : 'Не прочитано';
                    ?>
                    <tr>
                        <td style="padding: 0; text-align: center"><input type="checkbox" name="dell[]" value="<?=$com->id?>"></td>
                        <td><span><?=$i?></span></td>
                        <td>
                            <span><?=$com->name?></span>
                        </td>
                        <td><span><?=$com->phone?></span></td>
                        <td>
                            <span><?=$com->mail?></span>
                        </td>
                        <td>
                            <span><?echo($com->transfer == 0 ? "Нужен" : "Не нужен");?></span>
                        </td>
                        <td>
                            <span><?echo($com->s_wherefrom == 0 ? "-" : $com->wherefrom->title);?></span>
                        </td>
                        <td class="<?if($com->status == 1):?> status_read <?else:?> status_unread<?endif;?>">
                            <span><?=$status?></span>
                        </td>
                        <td>
                            <span><?=$com->s_time?></span>
                        </td>
                        <td>
                            <span><?=$com->s_date?></span>
                        </td>
                        <td>
                            <!--<a href="/state/edit/<?/*=$com->id*/?>"><img src="/img/edit.gif"></a>-->
                            <a href="/statement/view/<?=$com->id?>"><img src="/img/details.gif"></a>
                        </td>
                    </tr>
                <?endforeach;?>
            <?endif;?>
        </table>
        <!--<input type="submit" name="submit" value="Удалить виделенное"/>-->
    </form>
</div>