<h1>Новый номер:</h1>
<?if($alertMessage != null):?>
    <div class="errorMessage">
        <?=$alertMessage?>
    </div>
<?endif;?>
    <form action="" method="post" enctype="multipart/form-data">
        <div class="page settings tabs panel">
            <input type="submit" name="submit" value="Сохранить" >
        </div>
    <div class="page settings tabs review_comment">
        <div class="tab">Краткое</div>
        <div class="left">
            <label>Фотография:</label>
            <input type="file" name="photo[]" required  onchange="viewAddImg(this,event)">
            <label>Название:</label>
            <input type="text" name="title_little" id="title" required>
            <label>Краткое описание:</label>
            <textarea name="descr" required></textarea>
        </div>
        <div class="view_img">
            <h2>Картинка, которая будет добавлена:</h2>
        </div>
    </div>

    <div class="page settings tabs review_comment">
        <div class="tab">Полное</div>
        <div class="left">
            <label>Фотографии:</label>
            <div class="m_cont">
                <input type="file" name="photo[]" class="multiple" onchange='viewAddImg(this,event);'>
            </div>
            <div class="buttons">
                <div class="addInput">Добавить фотку</div>
            </div>
            <label>Название:</label>
            <input type="text" name="title" id="title" required>
            <label>Описание:</label>
            <textarea name="descr_all" required></textarea>
            <label>Цена:</label>
            <input type="text" name="price" id="price">
            <label>В номере есть: <br/>(нажмите на нужный пункт чтобы изменить и нажмите Enter, чтобы сохранить)</label>
            <ul class="list">

            </ul>
            <input type="text" id="roomh">
            <div class="buttons">
                <div class="addInputHave" onclick="addHave()">Добавить</div>
            </div>

            <label>Выбор категории: </label>
            <input type="radio" name="avaliable" id="avaliable"  value="1" checked>
            <label for="avaliable" id="avaliable_1">1-а категория</label>
            <div></div>
            <input type="radio" name="avaliable" id="unavaliable" value="2" >
            <label for="unavaliable" id="unavaliable_1">2-ая категория</label>
         </div>
        <div class="view_img_mult">
            <h2>Картинки, которые будут добавлены:</h2>
        </div>
    </div>
</form>