<h1>Общая зона</h1>
<div class="edit_recalls edit_our_works">
    <form method="post" action="common/delete/" class="delete_form">
        <?if($alertMessage != null):?>
            <div class="errorMessage">
                <?=$alertMessage?>
            </div>
        <?endif;?>
        <div class="page settings tabs panel">
            <a href="common/add/" class="subm_link">Новая зона</a>
            <div style="clear: both"></div>
        </div>
        <table>
            <?$i=0;?>
            <?if($comm_count == 0):?>
                <tr><th><h3>Данных для отображения нету :-(</h3></th></tr>
            <?else:?>
                <tr><th></th><th>#</th><th>Картинка</th><th>Название</th><th>Описание</th><th>Действия</th></tr>
                <?foreach($comm as $com): $i++;?>
                    <tr>
                        <td style="padding: 0; text-align: center"><input type="checkbox" name="dell[]" value="<?=$com->id?>"></td>
                        <td><span><?=$i?></span></td>
                        <td>
                            <img src="/img/uploads/common/<?=$com->image?>">
                        </td>
                        <td><span><?=$com->title?></span></td>
                        <td>
                            <span><?=$com->descr?></span>
                        </td>
                        <td>
                            <a href="common/edit/<?=$com->id?>"><img src="/img/edit.gif"></a>
                            <a href="common/delete/<?=$com->id?>"><img src="/img/delete.gif"></a>
                        </td>
                    </tr>
                <?endforeach;?>
            <?endif;?>
        </table>
        <input type="submit" name="submit" value="Удалить виделенное"/>
    </form>
</div>