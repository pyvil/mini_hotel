<?if(($subview == 'auth') || ($subview == 'landing')){
    include $subview.'.php';
    return;
}?>

<!DOCTYPE html>
<html>
	<head>
		<title>PHPixie</title>

        <link rel="stylesheet" href="/css/style.css">
        <link rel="stylesheet" href="/css/photoViewer.css">

        <script type="text/javascript" src="/js/jquery.min.js"></script>
        <script src="/js/jquery.maskedinput.js" type="text/javascript"></script>
        <script src="/js/text.js" type="text/javascript"></script>
        <script type="text/javascript" src="/js/photoViewer.js"></script>
        <script type="text/javascript" src="/js/script.js"></script>
	</head>
	<body>

    <div class="header_all">
        <div class="header">
            <div class="site_name">
                <h1><a href='/welcome'>Мини отель</a></h1>
            </div>
            <div class="user">
                <div class="username">
                    Добро пожаловать, <?=$username.' '.$userlastname?>
                </div>
                <div class="userSettings">
                    <ul>
                        <li><a href="/profile">Мои настройки</a></li>
                        <li><a href="/logout">Выйти</a></li>
                        <li><a href="/" target="_blank">Перейти на сайт</a></li>
                    </ul>
                </div>
            </div>
            <div style="clear: both"></div>
        </div>
    </div>

    <div class="menu">
        <ul>
                <li>
                    <a href="/settings">Настройки</a>
                </li>
                <li>
                    <a href="/advantages">Наши преимущества</a>
                </li>
                <li>
                    <a href="">Наши номера</a>
                    <ul>
                        <li><a href="/common">Общая зона</a></li>
                        <li><a href="/room">Номера</a></li>
                    </ul>
                </li>
                <li>
                    <a href="">Отзывы</a>
                    <ul>
                        <li><a href="/videorecall">Видео отзывы</a></li>
                        <li><a href="/recall">Текствые отзывы</a></li>
                    </ul>
                </li>
                <li>
                    <a href="/statement">Заявки</a>
                </li>
                <li>
                    <a href="/admins">Администрация</a>
                </li>
            <div style="clear: both"></div>
        </ul>
    </div>

    <div class="content">
		<?php include($subview.'.php');?>
    </div>

    <div class="footer">
        <p>Мини отель &copy; 2015</p>
        <p>
            <a href="/error">Сообщить об ошибке</a>
        </p>
    </div>
    <div class="back_to_top">
        <img src="/img/upp.png">
    </div>
	</body>
</html>