<?
    $status = $st->status == 1 ? "Прочитано" : 'Не прочитано';
?>

<div class="page review_comment">
    <form action="" method="post">
        <h1>Просмотр:</h1>
        <div class="order_item">
            <label>Имя: </label>
            <p><?=$st->name?></p>
        </div>
        <div class="line"></div>
        <div class="order_item">
            <label>Телефон: </label>
            <p><?=$st->phone?></p>
        </div>
        <div class="line"></div>
        <div class="order_item">
            <label>Статус: </label>
            <p class="<?if($st->status == 1):?> status_read <?else:?> status_unread<?endif;?>"><span><?=$status?></span></p>
        </div>
        <div class="line"></div>
        <div class="order_item">
            <label>Дата: </label>
            <p><?=$st->s_date?></p>
        </div>
        <input type="submit" name="submit" value="Отметить как не прочитаный">
    </form>
</div>