<h1>Новый отзыв:</h1>
<?if($alertMessage != null):?>
    <div class="errorMessage">
        <?=$alertMessage?>
    </div>
<?endif;?>
<div class="page review_comment">
    <form enctype="multipart/form-data" action="" method="post">
        <label>Видео (загрузите видео на <a href="https://www.youtube.com/" target="_blank">youtube.com</a>) и вставте ссылку на видео сюда:</label>
        <input type="text" id="src" required>
        <img src="/img/loader.gif" style="display: none" id="loader">
        <label>Название:</label>
        <input type="text" name="title" id="title" required>
        <input type="hidden" name="src" value="" id="h_src">
        <input type="submit" name="submit" value="Добавить" >
        <div style="clear: both"></div>
    </form>
    <div class="view_img">
        <h2>Видео:</h2>
    </div>
</div>