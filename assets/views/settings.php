<?
    $title      = '';
    $descr      = '';
    $usp        = '';
    $phone      = '';
    $alt_phone  = '';
    $avaliable  = '';
    $address    = '';
    $mail       = '';
    $id         = '';

    if($settings != null){
        foreach($settings as $setting):
            $title      = $setting->title;
            $descr      = $setting->descr;
            $usp        = $setting->usp;
            $phone      = $setting->phone;
            $alt_phone  = $setting->alt_phone;
            $avaliable  = $setting->avaliable;
            $address    = $setting->address;
            $mail       = $setting->mail;
            $id         = $setting->id;
        endforeach;
    }
?>

<h1>Настройки</h1>
<form method="post">
    <div class="page settings tabs panel">
        <input type="submit" name="submit" value="Сохранить" >
        <input type="hidden" name="id" value="<?=$id?>">
        <div style="clear: both"></div>
    </div>

    <div class="page settings tabs">
        <div class="tab">Название магазина</div>
        <label>Оглавление сайта: <span><span id="title_w"></span> символов</span></label>
        <input type="text" name="title" id="title" value="<?=$title?>" required>
        <label>Описание сайта: <span><span id="descr_w"></span> символов</span></label>
        <textarea name="descr" id="descr"  required><?=$descr?></textarea>
        <div style="clear: both"></div>
    </div>

    <div class="page settings tabs">
        <label>Сайт:</label>
        <input type="radio" name="avaliable" id="avaliable"  value="1" <?if($avaliable == '1'):?>checked<?endif?>>
        <label for="avaliable" id="avaliable_1">Включен</label>
        <div></div>
        <input type="radio" name="avaliable" id="unavaliable" value="0" <?if($avaliable == '0'):?>checked<?endif?>>
        <label for="unavaliable" id="unavaliable_1">Выключен</label>


        <div class="tab">Общее</div>
        <label>Уникальное торговое предложение</label>
        <textarea name="usp" required><?=$usp?></textarea>

        <label>Адресс</label>
        <input type="text" name="address" value="<?=$address?>" required>
        <label>E-mail</label>
        <input type="email" name="mail" value="<?=$mail?>">
        <label>Телефон</label>
        <input type="text" placeholder="+38(098) 765 4321" name="phone" id="phone" value="<?=$phone?>" required>
        <label>Дополнительный телефон</label>
        <input type="text" placeholder="+38(098) 765 4321" name="alt_phone" id="alt_phone" value="<?=$alt_phone?>">
    </div>
</form>