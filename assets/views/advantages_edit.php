<?
    $id     = '';
    $state  = '';
    $text   = '';
    $id = $adv['id'];
    $state = $adv['state'];
    $text = $adv['text'];
?>
<h1>Редаткировать преимущество №<?=$id?></h1>
<div class="edit_recalls edit_our_works">
    <form method="post" action="">
        <div class="page settings tabs panel">
            <input type="submit" name="submit" value="Сохранить" >
            <p class="delete_item"><a href="advantages/delete/<?=$id?>"><img src="/img/delete_item.png"></a></p>
        </div>
        <div class="page settings">
            <label>Статус </label>
            <input type="radio" name="avaliable" id="avaliable"  value="1" <?=($state==1) ? 'checked' : ''?>>
            <label for="avaliable" id="avaliable_1">Включен</label>
            <div></div>
            <input type="radio" name="avaliable" id="unavaliable" value="0" <?=($state==0) ? 'checked' : ''?>>
            <label for="unavaliable" id="unavaliable_1">Выключен</label>
            <div class="line"></div>
            <label>Текст преимущества </label>
            <textarea name="text" required><?=$text?></textarea>

        </div>
    </form>
</div>