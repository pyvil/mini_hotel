-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Хост: localhost
-- Час створення: Квт 02 2015 р., 18:39
-- Версія сервера: 5.5.41-0ubuntu0.14.04.1
-- Версія PHP: 5.5.9-1ubuntu4.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База даних: `mini_hotel`
--

-- --------------------------------------------------------

--
-- Структура таблиці `advantageses`
--

CREATE TABLE IF NOT EXISTS `advantageses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `state` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=8 ;

--
-- Дамп даних таблиці `advantageses`
--

INSERT INTO `advantageses` (`id`, `text`, `state`) VALUES
(3, 'sdf gds sdfgdsfgdsfgdfgbg gddh dhg', 1),
(4, 'вапвапыаапывп', 1),
(5, 'ывапывапвапмячсимичсми', 1),
(6, 'чсмиысмиываиыаивсм', 1),
(7, 'ваиываиваыиыв', 1);

-- --------------------------------------------------------

--
-- Структура таблиці `commons`
--

CREATE TABLE IF NOT EXISTS `commons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `descr` text NOT NULL,
  `image` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп даних таблиці `commons`
--

INSERT INTO `commons` (`id`, `title`, `descr`, `image`) VALUES
(1, 'зона1_11', 'This is it! Now we can manage our fairies!', 'zakat20.jpg'),
(2, 'зона2d', 'What we have here is a simple form that submits its data using the POST method back to the same page. The controller will have to check if the form is submitted and process it, otherwise it should just display it.', '8c097jmzlcw.jpg'),
(3, 'зона3', 'What we have here is a simple form that submits its data using the POST method back to the same page. The controller will have to check if the form is submitted and process it, otherwise it should just display it.', 'zakat31.jpg'),
(4, 'sdfbsdfb', 'dfbdsfb', 'zakat25.jpg');

-- --------------------------------------------------------

--
-- Структура таблиці `indexes`
--

CREATE TABLE IF NOT EXISTS `indexes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `lastname` varchar(100) NOT NULL,
  `mail` varchar(100) NOT NULL,
  `pass` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп даних таблиці `indexes`
--

INSERT INTO `indexes` (`id`, `name`, `lastname`, `mail`, `pass`) VALUES
(1, 'Виталик', 'Пятин', 'vitalik-1p@ukr.net', 'fender95');

-- --------------------------------------------------------

--
-- Структура таблиці `recalls`
--

CREATE TABLE IF NOT EXISTS `recalls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) NOT NULL,
  `text` text NOT NULL,
  `image` text NOT NULL,
  `who` varchar(128) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Дамп даних таблиці `recalls`
--

INSERT INTO `recalls` (`id`, `title`, `text`, `image`, `who`) VALUES
(4, '11', 'вапівап11', 'aircraft-of-world-war-ii-wallpaper-1366x768.jpg', '1111');

-- --------------------------------------------------------

--
-- Структура таблиці `roomhaves`
--

CREATE TABLE IF NOT EXISTS `roomhaves` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `text` text NOT NULL,
  `id_room` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `roompics`
--

CREATE TABLE IF NOT EXISTS `roompics` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `image` text NOT NULL,
  `id_room` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `rooms`
--

CREATE TABLE IF NOT EXISTS `rooms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title_little` varchar(64) NOT NULL,
  `title` varchar(128) NOT NULL,
  `descr` varchar(256) NOT NULL,
  `descr_all` text NOT NULL,
  `price` varchar(10) NOT NULL,
  `cat` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Структура таблиці `sitesettings`
--

CREATE TABLE IF NOT EXISTS `sitesettings` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` text NOT NULL,
  `descr` text NOT NULL,
  `usp` text NOT NULL COMMENT 'Unique Selling Proposition',
  `phone` varchar(30) NOT NULL,
  `alt_phone` varchar(30) NOT NULL,
  `address` text NOT NULL,
  `mail` varchar(100) NOT NULL,
  `avaliable` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп даних таблиці `sitesettings`
--

INSERT INTO `sitesettings` (`id`, `title`, `descr`, `usp`, `phone`, `alt_phone`, `address`, `mail`, `avaliable`) VALUES
(1, 'Mini hotel update', 'description', 'Unique Selling Proposition', '+38(063) 852-7410', '+38(098) 250-2002', 'Заречанская 8', 'vitalik-1p@ukr.net', 0);

-- --------------------------------------------------------

--
-- Структура таблиці `statements`
--

CREATE TABLE IF NOT EXISTS `statements` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `phone` varchar(100) NOT NULL,
  `mail` varchar(100) NOT NULL,
  `transfer` tinyint(1) NOT NULL,
  `s_wherefrom` int(11) NOT NULL,
  `s_time` time NOT NULL,
  `status` tinyint(1) NOT NULL,
  `s_date` date NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Дамп даних таблиці `statements`
--

INSERT INTO `statements` (`id`, `name`, `phone`, `mail`, `transfer`, `s_wherefrom`, `s_time`, `status`, `s_date`) VALUES
(1, 'йцуйуцуйцу', '0987654321', 'vitalik-1p@ukr.net', 0, 1, '00:00:00', 1, '2015-04-01');

-- --------------------------------------------------------

--
-- Структура таблиці `videorecalls`
--

CREATE TABLE IF NOT EXISTS `videorecalls` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(64) NOT NULL,
  `src` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп даних таблиці `videorecalls`
--

INSERT INTO `videorecalls` (`id`, `title`, `src`) VALUES
(1, 'David Gilmour - Another Brick In The Wall - Guitar Solo (Slow &a', 'gpctDVWC0Pw'),
(3, 'Прохождение Assassin&#39;s Creed Unity PS4 #16 - Финал - YouTube', '1bOEgUl959g');

-- --------------------------------------------------------

--
-- Структура таблиці `wherefroms`
--

CREATE TABLE IF NOT EXISTS `wherefroms` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп даних таблиці `wherefroms`
--

INSERT INTO `wherefroms` (`id`, `title`) VALUES
(1, 'Авто вокзал'),
(2, 'Ж/д вокзал'),
(3, 'Аэропорт');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
